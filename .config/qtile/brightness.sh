#!/usr/bin/env bash
if [ "$1" = "down" ]; then
	xbacklight -dec 5
    libnotify-notify-send -p -r 1 "Brightness: $(xbacklight -get)%"
elif [ "$1" = "up" ]; then
	xbacklight -inc 5
    libnotify-notify-send -p -r 1 "Brightness: $(xbacklight -get)%"
fi
