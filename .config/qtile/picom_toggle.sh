#!/bin/bash
pgrep picom >/dev/null &&  (killall picom && libnotify-notify-send "Killing Picom") || ( libnotify-notify-send "Spawning Picom" && picom -b )
