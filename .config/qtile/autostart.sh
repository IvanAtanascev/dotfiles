#!/usr/bin/env bash
#xinput --set-prop 8 'libinput Accel Profile Enabled' 0, 1
#xset -b
#xset dpms 0 0 0 && xset -dpms  && xset s off && xset s noblank
#xrandr --output DisplayPort-0 --primary --mode 2560x1440 --rate 143.91 --pos 0x0 --rotate normal --output DisplayPort-1 --mode 1920x1080 --rate 144.00 --pos 2560x360 --rotate normal --output DisplayPort-2 --off --output HDMI-A-0 --off
#corectrl --minimize-systray &
#/usr/bin/openrazer-daemon &
#polychromatic-helper --autostart &
export QT_QPA_PLATFORMTHEME=qt5ct
dunst &
picom -b
/usr/libexec/polkit-gnome-authentication-agent-1 &
xfce4-volumed-pulse &
flameshot &
nm-applet &
#udiskie --appindicator -t &
#/bin/sh
