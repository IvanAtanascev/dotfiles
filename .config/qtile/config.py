from libqtile import bar, layout, widget, qtile
from libqtile.config import Click, Drag, Group, Key, Match, Screen, ScratchPad, DropDown
from libqtile.lazy import lazy
import os
import subprocess
from libqtile import hook
from qtile_extras import widget
from qtile_extras.widget.decorations import PowerLineDecoration
from libqtile.utils import lazify_imports
from libqtile.backend.wayland import InputConfig

mod = "mod4"
terminal = "alacritty"
session_exclusive_keys = []
if qtile.core.name == "x11":
    backend = "x11"
    session_exclusive_keys = [
        Key(["mod1"], "p", lazy.spawn(
            "/home/ivan/.config/qtile/picom_toggle.sh"), desc="Toggle picom"),
        Key([mod], "d", lazy.spawn("rofi -show drun"), desc="Run launcher"),
        Key([], "Print", lazy.spawn("flameshot gui"), desc="Flameshot"),
    ]
elif qtile.core.name == "wayland":
    backend = "wayland"
    session_exclusive_keys = [
        Key([mod], "d", lazy.spawn("fuzzel"), desc="Run launcher"),
        Key(
            [],
            "print",
            lazy.spawn("/home/ivan/Scripts/screenshot/captureArea.sh"),
            desc="Screenshot",
        )
    ]


keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    Key([mod], "u", lazy.next_screen(), desc='Next monitor'),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.

    Key([mod], "i", lazy.layout.grow()),
    Key([mod], "m", lazy.layout.shrink()),
    Key([mod], "o", lazy.layout.maximize()),


    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.reset(), desc="Reset all window sizes"),
    Key([mod], "f", lazy.window.toggle_floating(),
        desc="Toggle window floating"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "END", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    Key([mod], "w", lazy.spawn("firefox-bin"), desc="Launch Firefox"),
    Key([mod], "e", lazy.spawn("thunar"), desc="Launch Thunar"),
    Key([mod], "b", lazy.hide_show_bar("top"), desc="Toggle top bar"),

    Key(
        [],
        "XF86AudioMute",
        lazy.spawn("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"),
        desc="Toggle audio mute"
    ),
    Key(
        [],
        "XF86AudioRaiseVolume",
        lazy.spawn("wpctl set-volume @DEFAULT_AUDIO_SINK@ 0.05+ --limit 1.0"),
        desc="Raise volume by 0.05"
    ),
    Key(
        [],
        "XF86AudioLowerVolume",
        lazy.spawn("wpctl set-volume @DEFAULT_AUDIO_SINK@ 0.05-"),
        desc="Lower volume by 0.05"
    ),
    Key(
        [],
        "XF86MonBrightnessDown",
        lazy.spawn("/home/ivan/.config/qtile/brightness.sh down"),
        desc="lower the brightness"
    ),
    Key(
        [],
        "XF86MonBrightnessUp",
        lazy.spawn("/home/ivan/.config/qtile/brightness.sh up"),
        desc="up the brightness"
    ),

    Key(["mod1"], "KP_Add", lazy.widget["keyboardlayout"].next_keyboard(),
        desc="Next keyboard layout."),
    Key([mod], '0', lazy.window.toggle_fullscreen(), desc="Toggle fullscreen",),
]

for key in session_exclusive_keys:
    keys.append(key)

group_attributes = {
    '1': {
        "label": "I.",
        "layout": "monadtall",
        "bind": "End"
    },
    '2': {
        "label": "II.",
        "layout": "monadtall",
        "bind": "Down"

    },
    '3': {
        "label": "III.",
        "layout": "monadtall",
        "bind": "Next"
    },
    '4': {
        "label": "IV.",
        "layout": "monadtall",
        "bind": "Left"
    },
    '5': {
        "label": "V.",
        "layout": "floating",
        "bind": "Begin"
    },
    '6': {
        "label": "VI.",
        "layout": "max",
        "bind": "Right"
    },
    '7': {
        "label": "VII.",
        "layout": "monadtall",
        "bind": "Home"
    },
    '8': {
        "label": "VIII.",
        "layout": "monadtall",
        "bind": "Up"
    },
    '9': {
        "label": "IX.",
        "layout": "monadtall",
        "bind": "Prior"
    },
}

groups = []

for i in group_attributes:
    groups.append(Group(
        i, label=group_attributes[i]["label"],
        layout=group_attributes[i]["layout"])
    )


for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                "KP_" + group_attributes[i.name]["bind"],
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                "KP_" + group_attributes[i.name]["bind"],
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(
                    i.name),
            ),
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(
                    i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

groups.append(
    ScratchPad('scratchpad', [
        DropDown(
            'terminal',
            'alacritty',
            height=0.60,
            width=0.70,
            x=0.15,
            y=0.05,
            opacity=1,
            on_focus_lost_hide=False,
            warp_pointer=False,
        )
    ])
)

keys.append(
    Key([mod], 'p', lazy.group['scratchpad'].dropdown_toggle('terminal')),
)

border_focus = "#b7b9e5"
border_normal = "#111111"


floating_layout = layout.Floating(
    border_focus=border_focus,
    border_normal=border_normal,
    border_width=2,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ],
)


layouts = [
    # layout.Columns(border_width=3, margin=8, margin_on_single=0,border_normal=border_normal, border_focus=border_focus),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Spiral(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4, margin=8, margin_on_single=0),
    layout.Bsp(border_width=2, margin=8, margin_on_single=0,
               border_normal=border_normal, border_focus=border_focus),
    # layout.Matrix(),
    layout.MonadTall(
        border_normal=border_normal,
        border_focus=border_focus,
        border_width=2,
        margin=12,
        single_margin=0,
        single_border_width=0),
    floating_layout,
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]


widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

powerline = {
    "decorations": [
        PowerLineDecoration(path="forward_slash")
    ]
}


def systray_init(
        background,
        foreground,
        font,
        fontsize,
        backend,
):
    if backend == "x11":
        return widget.Systray(
            foreground=foreground,
            font=font,
            fontsize=fontsize,
            **powerline
        )
    else:
        return widget.StatusNotifier(
            background="#7aa2f7cc",
            foreground=foreground,
            font=font,
            fontsize=fontsize,
            **powerline
        )


def init_widgets_list(font, fontsize=10, foreground="#c0caf5",
                      background="#15161e"):
    widget_list = [
        widget.GroupBox(
            background=background,
            font=font,
            foreground=foreground,
            fontsize=fontsize,
            highlight_method="text",
            this_current_screen_border="#97729d",
            this_screen_border="#aaaaaa",
            active=foreground,
            inactive="#777777",
        ),
        widget.Battery(
            background=background,
            font=font,
            foreground=foreground,
            fontsize=fontsize,
        ),
        widget.WindowName(
            background=background,
            foreground=foreground,
            font=font,
            fontsize=fontsize,
        ),
        widget.StatusNotifier(
            background=background,
            foreground=foreground,
            font=font,
            fontsize=fontsize,
        ),
        widget.Prompt(font=font, fontsize=fontsize, foreground=foreground),
        widget.CurrentLayout(font=font,
                             fontsize=fontsize,
                             foreground=foreground,
                             background=background,
                             ),
        widget.Volume(font=font,
                      fontsize=fontsize,
                      foreground=foreground,
                      background=background,
                      ),
        widget.KeyboardLayout(configured_keyboards=[
                              'us', 'cz'],
                              font=font,
                              fontsize=fontsize,
                              foreground=foreground,
                              background=background,
                              ),
        widget.Clock(format="%d-%m-%Y %a %H:%M",
                     font=font,
                     fontsize=fontsize,
                     foreground=foreground,
                     background=background,
                     ),
    ]

    return widget_list


background = "#00000099"


def init_widgets_screen1(font):
    widget_list = init_widgets_list(
        background=background, font=font, fontsize=14)
    return widget_list


def init_screens(wallpaper, font):
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(font), size=27,
                               background="#00000000"),
                   wallpaper=wallpaper,
                   wallpaper_mode="fill",
                   ),
            ]


wallpaper = "~/Pictures/flooded_cathedral.png"
font = "Fira Bold"
screens = init_screens(wallpaper=wallpaper, font=font)
# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = False

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = {
    "5426:92:Razer Razer DeathAdder Elite": InputConfig(
        accel_profile='flat'
    ),
}
# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"


@hook.subscribe.startup_once
def autostart():
    if backend == "x11":
        home = os.path.expanduser("~/.config/qtile/autostart.sh")
        subprocess.Popen([home])
    elif backend == "wayland":
        home = os.path.expanduser("~/.config/qtile/wautostart.sh")
        subprocess.Popen([home])
